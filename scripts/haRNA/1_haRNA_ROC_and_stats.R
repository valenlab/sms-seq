rm(list=ls(all = TRUE))
gc(reset=TRUE)

library(data.table)
library(ggplot2)

source("scripts/functions.R")
threshold <- 0.2

# Load haRNA the same way as in read_medium_hairpin_filtered()
# but keep read_mrate for plots

haRNA <- fread("data/haRNA.csv")
haRNA <- subset(haRNA, base == "A" & haRNA == "medium" & replicate == "R3")
# everything in the pCS2 is open too
haRNA[pos < 29, "struct"] <- 1

read_mrate <- haRNA[, .(score = sum(stat < threshold)/.N, cvg = .N, mods = sum(stat < threshold)), 
                    by = c("read_id")][order(-score)]
haRNA$is_mod <- haRNA$stat < 0.2

# what is modification rate for open positions, for closed?
haRNA$is_loop <- factor(haRNA$struct, levels = c(0, 1), labels = c("stem", "loop"))

library(cutpointr)
spec_constrain_adj <- function(tp, fp, tn, fn, ...) {
  cutpointr::spec_constrain(tp, fp, tn, fn, min_constrain = 0.3)
}

summary <- function(data, opt_cut = NULL) {
  cp <- cutpointr(x = data$stat, # probability of being a stem
                  class = data$is_loop,
                  method = maximize_metric,
                  # given minimum sens get max spec
                  metric = spec_constrain_adj, # cutpointr::cohens_kappa, #
                  # x is supposed to be smaller (<=) for pos_class
                  pos_class = "loop", direction = "<=", na.rm = TRUE)
  roc <- cp$roc_curve[[1]]
  roc$By <- "read"
  # if we treat our data averaged per position
  #  data_avg <- data[, .(stat = mean(stat)), by = c("is_loop", "pos")]
  data_avg <- data %>% group_by(pos, is_loop) %>% summarise(stat = sum(stat < threshold) / n())
  data_avg <- data.table(data_avg)
  cp_avg <- cutpointr(x = data_avg$stat, # Frequency of modified bases
                      class = data_avg$is_loop,
                      method = maximize_metric,
                      # given minimum sens get max spec
                      metric = spec_constrain_adj, # cutpointr::cohens_kappa, #
                      # x is supposed to be smaller (<=) for pos_class
                      pos_class = "stem", direction = "<=", na.rm = TRUE)
  roc_avg <- cp_avg$roc_curve[[1]]
  roc_avg$By <- "consensus"
  roc <- rbind(roc_avg, roc)
  
  p <- ggplot(roc, aes(x = 1 - tnr, y = tpr, color = By)) + 
    geom_line(lwd=0.3) +
    ggthemes::theme_base() +
    geom_segment(aes(x = 0, xend = 1, y = 0, yend = 1), color = "black", linetype = "dashed", lwd=0.3) +
    labs(x = "1 - Specificity", y = "Sensitivity") +
    ggthemes::scale_color_colorblind() +
    xlim(0, 1)
  p <- p + theme_bw() +
    theme(legend.position="none", axis.title = element_blank(), panel.grid = element_blank())  
  ggsave("figures/haRNA/haRNA_roc.png", p, width = 2, height = 1.2)
  ggsave("figures/haRNA/haRNA_roc.pdf", p, width = 2, height = 1.2)
  
  
  p <- ggplot(data_avg, aes(as.factor(pos), stat, fill=is_loop)) + 
    geom_bar(stat="identity") + theme_bw(base_size = 6) + 
    scale_fill_manual(values=c(stem_color, loop_color))  +
    ylab("Modification Frequency") + xlab("Position") + 
    theme(legend.position="none")
  ggsave("figures/haRNA/haRNA_barchart.png", p, width = 7, height = 7)
  ggsave("figures/haRNA/haRNA_barchart.pdf", p, width = 7, height = 7)
  ggsave("figures/haRNA/haRNA_barchart.pdf", p, width = 2, height = 1.2)
  
  if (is.null(opt_cut)) opt_cut <- cp$optimal_cutpoint
  pred_loop <- data$stat <= opt_cut
  real_loop <- data$is_loop == "loop"
  tp <- as.numeric(sum(real_loop & pred_loop))
  tn <- as.numeric(sum(!real_loop & !pred_loop))
  fp <- as.numeric(sum(!real_loop & pred_loop))
  fn <- as.numeric(sum(real_loop & !pred_loop))
  mcc <- (tp * tn - fp * fn) / sqrt((tp + fp) * (tp + fn) * (tn + fp) * (tn + fn))
  out <- c(cutpointr::cohens_kappa(tp, fp, tn, fn),
           (tp + tn) / (tp + fp + tn + fn), # acc
           tn / (tn + fp), # spec
           tp / (tp + fn), # sens
           tp / (tp + fp), # precision
           mcc,
           opt_cut)
  names(out) <- c("Kappa", "Acc", "Spec", "Sens", "Precision", "MCC", "Cutpoint")
  
  out
}
#summary(haRNA)
summary(haRNA, opt_cut = threshold)

### Figure 1C
haRNA[, .(score = sum(stat < threshold)/.N, cvg = .N), by = c("struct")]
hdat <- haRNA
hdat[hdat$struct == 1, "struct"] = "Open"
hdat[hdat$struct == 0, "struct"] = "Stem"
p <- ggplot(hdat, aes(x = struct, y = stat, fill=struct)) + 
  geom_violin(lwd = 0.3) + 
  scale_fill_manual(values = c(loop_color, stem_color)) + 
  theme_bw(base_size = 12) +
  geom_hline(yintercept = threshold, linetype = "dotted") + 
  theme(legend.position = "none", axis.title = element_blank(), panel.grid = element_blank())  
p
ggsave("figures/haRNA/haRNA_violin.png", p, width = 2, height = 1.2)
ggsave("figures/haRNA/haRNA_violin.pdf", p, width = 2, height = 1.2)
ggsave("figures/haRNA/haRNA_violin.pdf", p, width = 5, height = 5, dpi = 300, unit = "cm")

## Supp figure 3
p <- ggplot(subset(read_mrate, cvg == 20), aes(mods)) + 
  geom_bar() + 
  theme_bw() + 
  geom_vline(xintercept=c(3.5, 18.5), color="red", linetype="dashed") +
  xlab("Modified bases") + ylab("Read count")
p
ggsave("figures/haRNA/modifications_per_read.png", p, width = 7, height = 5)
ggsave("figures/haRNA/modifications_per_read.pdf", p, width = 7, height = 5)


## GFP vs hairpin
haRNA <- read.delim("data/hairpinRNA_tombo.txt")
r <- data.frame(xmin = 0, xmax = 306, ymin = 0.23, 
                ymax = 0.2, position = NA, difference = NA)
p <- ggplot(subset(haRNA, position>10), aes(x=position, y=difference)) + 
  geom_line() + 
  theme_bw() + 
  geom_vline(xintercept = 306, color="red") + 
  ylab("Control - Modified") 
ggsave("figures/haRNA/Modified_vs_unmodified.pdf", width=6, height=5, plot=p)
ggsave("figures/haRNA/Modified_vs_unmodified.png", width=6, height=5, plot=p)