rm(list=ls(all = TRUE))
gc(reset=TRUE)

library(grid)
library(gridExtra)

t <- 0.2
min_pos <- 17
max_pos <- 322

source("scripts/functions.R")
noncoding <- data.frame(read_noncoding())

setDT(noncoding)
st <- noncoding[, .(read_count = length(unique(read_id))), by = seqnames][order(-read_count)]
fwrite(st, "~/noncoding_by_count.csv")

snR17a_seq <- "GTCGACGTACTTCATAGGATCATTTCTATAGGAATCGTCACTCTTTGACTCTTCAAAAGAGCCACTGAATCCAACTTGGTTGATGAGTCCCATAACCTTTGTACCCCAGAGTGAGAAACCGAAATTGAATCTAAATTAGCTTGGTCCGCAATCCTTAGCGGTTCGGCCATCTATAATTTTGAATAAAAATTTTGCTTTGCCGTTGCATTTGTAGTTTTTTCCTTTGGAAGTAATTACAATATTTTATGGCGCGATGATCTTGACCCATCCTATGTACTTCTTTTTTGAAGGGATAGGGCTCTATGGGTGGGTACAAATGGCAGTCTGACAAGT"

# RNAfold -p --MEA=0.1 --outfile=snR17a.mea01.output snR17a.fa
snR17a_structs_file <- "struct/yeast/in_silico_fold/snR17a.mea01.output"
snR17a_structs <- read_RNAFold_dotBracket(snR17a_structs_file)
snR17a <- subset(noncoding, seqnames=="snR17a")
snR17aA <- subset(snR17a, base == "A")

# no guarantees that there will be n chunks of closed regions that span across fraction
make_closed_chunks <- function(n = 10, fraction = 0.5, start = 1, stop = 333, seed = 42) {
  set.seed(seed)
  rand_strt <- sort(sample(start:stop, n))
  init <- sample(2:(length(rand_strt) - 1), 1) # this is starting moment of distributing closed extensions
  
  fract <- floor((stop - start + 1) * fraction) # number of position to close
  widths <- rep(1, length(rand_strt))
  for (i in init:length(rand_strt)) {
    if (i == length(rand_strt)) {
      ma <- stop - rand_strt[i]
    } else {
      ma <- rand_strt[i+1] - rand_strt[i]
    }
    if (ma > fract) ma <- fract
    widths[i] <- sample(1:ma, 1)
    fract <- fract - widths[i]
  }
  
  for (i in 1:(init-1)) {
    ma <- rand_strt[i+1] - rand_strt[i]
    if (ma > fract) ma <- fract
    widths[i] <- sample(1:ma, 1)
    fract <- fract - widths[i]
  }
  
  closed <- rep(".", stop)
  for (i in 1:length(rand_strt)){
    closed[rand_strt[i]:(rand_strt[i] + widths[i] - 1)] <- "("
  }
  closed
  return(closed)
}
randomize_db <- function(struct, closed_chunks, closed_fraction, names, seed = 42) {
  mi <- min(struct$pos)
  ma <- max(struct$pos)
  # # for 3 closed_chunks randomize some structures
  # struct[struct$Structure == "MFE", ]$db <- make_closed_chunks(
  #   n = closed_chunks[1], closed_fraction[1], mi, ma, seed)
  # struct[struct$Structure == "Centroid", ]$db <- make_closed_chunks(
  #   n = closed_chunks[2], closed_fraction[2], mi, ma, seed)
  # struct[struct$Structure == "MEA", ]$db <- make_closed_chunks(
  #   n = closed_chunks[3], closed_fraction[3], mi, ma, seed)
  
  # just shuffle
  set.seed(seed)
  struct[struct$Structure == "MFE", ]$db <- sample(struct[struct$Structure == "MFE", ]$db)
  struct[struct$Structure == "Centroid", ]$db <- sample(struct[struct$Structure == "Centroid", ]$db)
  struct[struct$Structure == "MEA", ]$db <- sample(struct[struct$Structure == "MEA", ]$db)
  
  struct$Structure[struct$Structure == "MFE"] <- names[1]
  struct$Structure[struct$Structure == "MEA"] <- names[2]
  struct$Structure[struct$Structure == "Centroid"] <- names[3]
  return(struct)
}


# iterations <- list()
# for (i in 1:10000) {
#   print(i)
#   snR17a_structs2 <- randomize_db(snR17a_structs, closed_chunks = c(10, 20, 30), 
#                                   closed_fraction = c(0.5, 0.5, 0.5), names = c("R1", "R2", "R3"), i)
#   mfr <- assign_reads_to_structure(snR17a_seq, snR17aA, 
#                                    subset(rbind(snR17a_structs, snR17a_structs2), 
#                                           Structure != "UNK" & seq == "A"), 
#                                    t, scol=4, min=min_pos, max=max_pos, read_count_only = T)
#   mfr$iteration <- i
#   iterations[[i]] <- mfr
# }
# 
# iterations2 <- rbindlist(iterations)
# fwrite(iterations2, "data/snoRNA17a_struct_permutations.csv")

# compare whether they are significantly different (dominated)
#ggplot(iterations2, aes(n_reads, fill=Structure)) + geom_density(alpha=0.5)

# t-test
# tt <- reshape(iterations2, idvar = "iteration", timevar = "Structure", direction = "wide")
# t.test(tt$n_reads.MEA, tt$n_reads.R2)
# t.test(tt$n_reads.MFE, tt$n_reads.R1)
# t.test(tt$n_reads.Centroid, tt$n_reads.R3)


snR17a_gr <- heatmaps_by_structure(snR17a_seq, snR17aA, snR17a_structs, 
                                   min=min_pos, max=max_pos, threshold=t, text=FALSE)
# seq = snR17a_seq; stat = snR17aA; reads <- stat; structs = snR17a_structs; threshold=0.2; min=min_pos; max=max_pos; scol=4; text=F;

ggplot()
grid.draw(snR17a_gr)
ggsave(paste0("figures/yeast/rna/snR17a_structures_t", t,".pdf"),  width=20, height=10, plot=snR17a_gr)
ggsave(paste0("figures/yeast/rna/snR17a_structures_t", t,".png"),  width=20, height=10, plot=snR17a_gr)
ggsave(paste0("figures/yeast/rna/snR17a_structures_t", t,".svg"),  width=20, height=10, plot=snR17a_gr)

snR17a_modFreq <- modFreq_by_structure(snR17a_seq, snR17aA, snR17a_structs, threshold=t, min=min_pos, max=max_pos)

#MEA
snR17a_seq
str_c(sapply(subset(snR17a_structs, Structure=="MEA")[,"db"], as.character), collapse="")
cat(subset(snR17a_modFreq, Structure=="MEA")[,"modFreq"])
cat(sapply(subset(snR17a_modFreq, Structure=="MEA")[,"modFreqFC"], function(x) {max(0,x)} )) ## FC - pos
cat(abs(sapply(subset(snR17a_modFreq, Structure=="MEA")[,"modFreqFC"], function(x) {min(0,x)} ))) ## FC - pos

#MFE
snR17a_seq
str_c(sapply(subset(snR17a_structs, Structure=="MFE")[,"db"], as.character), collapse="")
cat(subset(snR17a_modFreq, Structure=="MFE")[,"modFreq"])
cat(sapply(subset(snR17a_modFreq, Structure=="MFE")[,"modFreqFC"], function(x) {max(0,x)} )) ## FC - pos
cat(abs(sapply(subset(snR17a_modFreq, Structure=="MFE")[,"modFreqFC"], function(x) {min(0,x)} ))) ## FC - pos

#Centroid
snR17a_seq
str_c(sapply(subset(snR17a_structs, Structure=="Centroid")[,"db"], as.character), collapse="")
cat(subset(snR17a_modFreq, Structure=="Centroid")[,"modFreq"])
cat(sapply(subset(snR17a_modFreq, Structure=="Centroid")[,"modFreqFC"], function(x) {max(0,x)} )) ## FC - pos
cat(abs(sapply(subset(snR17a_modFreq, Structure=="Centroid")[,"modFreqFC"], function(x) {min(0,x)} ))) ## FC - pos
