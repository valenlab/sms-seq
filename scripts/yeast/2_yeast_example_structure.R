rm(list=ls(all = TRUE))
gc(reset=TRUE)

# Not thrash

library(ORFik)
library(data.table)
library(rtracklayer)
library(GenomicFeatures)
library(GenomicRanges)
library(ggplot2)


## SELECTED STRUCTURE
rna_id <- "YPL143W"
seq <- "GGGGUUCUAAGAUUAGAGUUAUGUGGGGUAAGGUUACCAGAACUCACGGUAACUCUGGUGUCGUUAGAGCCACCUU"
str <- "(((((((((((((((((((((((((((...............))))).)))))))))..))).)))))))).)).."
threshold <- 0.2

source("scripts/functions.R")

stat <- read_mRNA(onlyA = T, F)
stat <- stat[seqnames == rna_id, ]

# filter out completely unmodified and almost completely modified reads
read_mrate <- stat[, .(stat = sum(stat < threshold)/.N, cvg = .N), 
                   by = c("read_id", "rep")][order(-stat)]
zero_one <- read_mrate$read_id[read_mrate$stat > 0.9 | read_mrate$stat < 0.1]
stat <- stat[!read_id %in% zero_one, ]

statA <- stat[, .(stat = sum(stat < threshold)/.N, cvg = .N), by = c("seqnames", "pos")]
statA <- statA[complete.cases(stat), ]
statA <- statA[cvg > 50, ]

s <- 236
e <- 312
struct_range <- s:e
s_tx <- statA[seqnames == rna_id, ]
s_tx <- s_tx[pos %in% struct_range, ]
s_tx$start <- s_tx$pos - s-1 # - 5' utr of 58 (utr3 of 158, cds 324)
p <- ggplot(s_tx, aes(x = start, y = stat)) +
  geom_vline(xintercept = c(30, 42), linetype = "dashed") +
  geom_line() +
  rxtras::theme_science(base_size = 16) +
  ggthemes::scale_color_colorblind() +
  labs(x = "Relative position of YPL143W", y = "Averaged modification level")
p
ggsave("figures/yeast/yeast_YPL143W.png", p)
ggsave("figures/yeast/yeast_YPL143W.pdf", p)


## for forna
statSum <- stat[, .(modFreq = sum(stat < threshold)/.N, cvg = .N), 
                by = c("seqnames", "pos")]
s <- 236
e <- 312
struct_range <- s:e
mf <- statSum[seqnames == rna_id, ]
mf <- mf[pos %in% struct_range, ]
mf$start <- mf$pos - s-1 # - 5' utr of 58 (utr3 of 158, cds 324)

mf[mf$base != "A", "modFreq"] <- 0.16
cat(mf$modFreq)