
This repository contains code used to create figures for SMS-seq paper.  

Data however is not uploaded and need to be pasted into the correct folders 
inside data directory. Most of the code is inside the "scripts" folder - see 
there for relevant code.