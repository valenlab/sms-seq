from tombo import tombo_helper
import pandas as pd
import sys

idx_file = sys.argv[1]
name = sys.argv[2]
start = int(sys.argv[3])
stop = int(sys.argv[4])
out_name = sys.argv[5]

reg_data = tombo_helper.intervalData(chrm=name, start=start, end=stop, strand='+')

def write_raw(file_path, cs_reads, start=start, end=stop):
    for read in cs_reads:
        if not read[2]:  # only unfiltered reads
            cs = tombo_helper.get_multiple_slots_read_centric(
                read, slot_names=["norm_mean", "norm_stdev", "start", "length", "base"])
            df = pd.DataFrame(
                data={'norm_mean': cs[0], 'norm_stdev': cs[1], 'start': cs[2], 'length': cs[3], 'base': cs[4],
                      'pos': list(range(read[0], read[1]))})
            df = df.loc[df['pos'].isin(list(range(start, end + 1)))]
            df['read_id'] = read[10]
            df.to_csv(file_path, mode='a', header=False, index=False)

    return

reads_index = tombo_helper.TomboReads([idx_file, ])
cs_reads = reads_index.get_cs_reads(name, '+')
write_raw(out_name, cs_reads)
