from tombo import tombo_helper
import pandas as pd
import sys

idx_file = sys.argv[1] # path to fast5 folder
name = sys.argv[2] 
out_name = sys.argv[3]

reads_index = tombo_helper.TomboReads([idx_file, ])
cs_reads = reads_index.get_cs_reads(name, '+')
read_id = list()
res_score = list()
q_score = list()
for read in cs_reads:
    if not read[2]:
        read_id.append(read[10])
        res_score.append(read[8])
        q_score.append(read[9])

df = pd.DataFrame(data={'read_id': read_id, 'res_score': res_score, 'mean_q_score': q_score})
df.to_csv(out_name, index=False)
