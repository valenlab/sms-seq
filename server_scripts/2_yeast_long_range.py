#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import logging
import pandas as pd
import multiprocessing as mp
import numpy as np
from sklearn.metrics.cluster import adjusted_mutual_info_score
from sklearn.metrics.cluster import mutual_info_score
import itertools

stat = sys.argv[1]
ids = sys.argv[2]
filter = int(sys.argv[3])
output = sys.argv[4]

def byTx(tx_stat):
  #tx_stat = stat[stat.seqnames == tx]
  tx_pos = tx_stat.groupby("start").agg({
          "read_id": lambda x: len(list(set(x)))})
  tx_pos = list(tx_pos.index[tx_pos.read_id > filter])
  
  if len(tx_pos) < 1:
      return pd.DataFrame()
  
  tx_pos = list(itertools.combinations(tx_pos, 2))
  txDT = dict.fromkeys(set(range(len(tx_pos) - 1)))
  for w in range(len(tx_pos) - 1):
    p1, p2 = tx_pos[w]     
    p1 = tx_stat[tx_stat.start == p1]
    p2 = tx_stat[tx_stat.start == p2]
    p1 = p1[p1.read_id.isin(p2.read_id)].sort_values(by=['read_id'])
    p2 = p2[p2.read_id.isin(p1.read_id)].sort_values(by=['read_id'])
    
    p1 = p1.is_mod.values
    p2 = p2.is_mod.values
    
    filt = np.logical_not(np.logical_and(np.logical_not(p1), np.logical_not(p2)))
    txDT[w] = [sum(np.logical_and(p1[filt], p2[filt]))/sum(filt) if sum(filt) != 0 else np.nan,                                     # co-open
               sum(p1 == p2)/len(p1) if len(p1) > 0 else np.nan,                                                                    # co-agreement
               mutual_info_score(p1, p2), adjusted_mutual_info_score(p1, p2), len(p2), tx_pos[w][0], tx_pos[w][1]]


  txDT = pd.DataFrame.from_dict(txDT, orient='index')
  txDT['seqnames'] = tx_stat.seqnames.values[0]
  print(tx_stat.seqnames.values[0])
  return txDT

def f_byTx(utx):
  try:
    return byTx(utx)
  except Exception as e:
    logging.exception(e, str(utx.seqnames.values[0]))

if __name__ == '__main__':
  pool = mp.Pool(processes = (mp.cpu_count() - 1))

  stat = pd.read_csv(stat)
  stat.columns = ["start", "score", "read_id", "seqnames"]
  stat["is_mod"] = stat["score"] < 0.2
  ids = pd.read_csv(ids)
  ids = ids.drop_duplicates()
  stat = pd.merge(stat, ids, on="read_id", how='inner')

  utx = list(set(stat.seqnames))
  gb = stat.groupby("seqnames")
  utx = [gb.get_group(x) for x in gb.groups]
  print(len(gb.groups))
  results = pool.map(f_byTx, utx)
  pool.close()
  pool.join()

  finalDT = pd.concat(results, ignore_index=True)
  finalDT.columns = ["co_open", "co_agree", "mutual_info", "adj_mutual_info", "cvg", "pos1", "pos2", "seqnames"]
  finalDT.to_csv(output, 
                 encoding='utf-8', index=False)
